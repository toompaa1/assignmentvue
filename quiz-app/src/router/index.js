import Vue from 'vue';
import VueRouter from 'vue-router';
import StartPage from '../components/startpage/StartPage.vue';
import QuizPage from '../components/quizpage/QuizPage.vue';
import ResultPage from '../components/resultpage/ResultPage.vue'

Vue.use(VueRouter)

/*
    Here we set the routes for each component.
*/
const routes = [
    {
        name: 'startpage',
        path: '/',
        component: StartPage
    },
    {
        name: 'quiz',
        path: '/quiz',
        component: QuizPage,
        props: true
    },
    {
        name: 'result',
        path: '/result',
        component: ResultPage,
        props: true
    }
]

const router = new VueRouter({
    routes
})

export default router