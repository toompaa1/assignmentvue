const GET_CATEGORY_URL = 'https://opentdb.com/api_category.php';
const GET_QUIZ_URL = 'https://opentdb.com/api.php?amount=';

/*
  Get the category data from the api.
*/
export const getCategory = () => {
  return fetch(`${GET_CATEGORY_URL}`)
    .then((res) => res.json())
    .then((data) => data);
};

/*
  Get the quiz data from the api and then checks if category and difficulty is "" then just return an empty string.
*/
export const getQuiz = (difficulty, number, category) => {
  let categoryid = `&category=${category}`;
  let difficult = `&difficulty=${difficulty}`;
  if (category === 8) {
    categoryid = '';
  }
  if (difficulty === '') {
    difficult = '';
  }
  return fetch(GET_QUIZ_URL + number + categoryid + difficult)
    .then((res) => res.json())
    .then((data) => data);
};
