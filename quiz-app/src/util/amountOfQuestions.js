/*
    Sets the amount of question for the select.
*/
const NUMBER_OF_QUESTION = [
    {
        "number": 5,
    },
    {
        "number": 10,
    },
    {
        "number": 20,
    },
    {
        "number": 30,
    },
    {
        "number": 40,
    },
    {
        "number": 50,
    },
];

export default NUMBER_OF_QUESTION;

