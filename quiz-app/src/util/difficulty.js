/*
    Sets the difficulties of question for the select.
*/
const DIFFICULTIES = [
    {
        "type": "any difficulty"
    },
    {
        "type": "easy"
    },
    {
        "type": "medium"
    },
    {
        "type": "hard"
    }
]

export default DIFFICULTIES;